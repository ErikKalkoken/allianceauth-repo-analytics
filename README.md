# allianceauth-repo-analytics

This repo contains data analytics about the Alliance Auth repository.

Here is an example chart:

![example](https://i.imgur.com/JJMyemp.png)

You will need [Jupyter Notebook](https://jupyter.org/) to run the analytics.
